﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace Exercise.Viewmodels
{
    public class MainPageViewModel : INotifyPropertyChanged
    {

        private string firstName = "John";
        private string surname = "Doe";
        private string email;
        private string favouriteAnimal = Properties.Resources.Dog;
        
        private DateTime birthday = new DateTime(1990, 1, 1);
        private int age;
        private string bioText;

        public MainPageViewModel()
        {
            age = (int)Math.Floor((DateTime.Now - birthday).TotalDays / 365);
        }

        public String FirstName {
            get {
                return firstName;
            }
            set {
                if (firstName != value)
                {
                    firstName = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("FirstName"));
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("BioText"));
                }
            }
        }

        public String Surname {
            get {
                return surname;
            }
            set {
                if (surname != value)
                {
                    surname = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Surname"));
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("BioText"));
                }
            }
        }

        public String Email {
            get {
                return email;
            }
            set {
                if (email != value)
                {
                    email = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Email"));
                }
            }
        }

        public DateTime Birthday {
            get {
                return birthday;
            }
            set {
                if (birthday != value)
                {
                    birthday = value;
                    Age = (int)Math.Floor((DateTime.Now - birthday).TotalDays / 365);
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Birthday"));
                }
            }
        }

        public int Age {
            get {
                return age;
            }
            set {
                Console.WriteLine("Age setter called: " + value);
                if (age != value)
                {
                    age = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Age"));
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("BioText"));
                }
            }
        }

        public String FavouriteAnimal {
            get {
                return favouriteAnimal;
            }
            set {
                if (favouriteAnimal != value)
                {
                    favouriteAnimal = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("FavouriteAnimal"));
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("BioText"));
                }
            }
        }

        public String BioText {
            get {
                return String.Format(Properties.Resources.BioText, firstName, surname, age, favouriteAnimal.ToLower());
            }
        }

        public List<string> Animals { get; } = new List<string>
    {
        "Dog",
        "Cat",
        "Duck",
        "Axolotl",
        "Bear",
        "Platypus",
        "Rhino"
    };

        public List<string> LocalizedAnimals { get; } = new List<string>
        {
            Properties.Resources.Dog,
            Properties.Resources.Cat,
            Properties.Resources.Duck,
            Properties.Resources.Axolotl,
            Properties.Resources.Bear,
            Properties.Resources.Platypus,
            Properties.Resources.Rhino
        };

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
